/*********************************************
 * OPL 12.8.0.0 Model
 * Author: ivan
 * Creation Date: Apr 6, 2018 at 11:55:13 PM
 *********************************************/
{string} PRENDAS = ...;
float TIEMPO_LAVADO[PRENDAS] = ...;
{string} LAVADOS = ...;
int INCOMPATIBILIDADES[PRENDAS, PRENDAS] = ...;

dvar boolean Prenda[PRENDAS, LAVADOS];
dvar boolean Lavado[LAVADOS];
dvar float+ T_Lavado[LAVADOS];
 
 minimize
 	sum(l in LAVADOS) T_Lavado[l];
 	
 subject to {
 	forall (p in PRENDAS) {
 		unLavado: sum (l in LAVADOS) Prenda[p,l] == 1; 		
 	}
 	forall (l in LAVADOS) {
		forall(i in PRENDAS) {
			forall(j in PRENDAS:INCOMPATIBILIDADES[i,j]==1) {
				incompatibilidades: Prenda[i,l] + Prenda[j,l] <= Lavado[l];
 			}
 		}
 		no_prendo: sum(p in PRENDAS) Prenda[p,l] <= Lavado[l]*card(PRENDAS);
 	}
 	forall(l in LAVADOS) {
 		forall(p in PRENDAS) {
 			tiempoLavado: T_Lavado[l] >= TIEMPO_LAVADO[p]*Prenda[p,l];
 		} 
 	}
 }
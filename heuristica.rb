require 'active_support/all'
require 'getoptlong'
require 'logger'

SORT_FACTORS = { asc: 1.0, desc: -1.0 }

def measure(thing_measured = nil, time_title = 'Time Elapsed')
	t1 = Time.now
	yield
	t2 = Time.now
	return Time.at(t2 - t1) if thing_measured.nil?
	thing_measured += ' - ' unless thing_measured == ''
	$log.info "#{thing_measured}#{time_title}: #{Time.at(t2 - t1).utc.strftime '%H hours, %M minutes, %S seconds, %L Miliseconds'}"
end

def solve(input_file, sort_mode, sort_order)
	file = File.read(input_file).split("\n")
	incompatibilities = file.select {|e| e.start_with? 'e'}
	                        .map { |e| e.split(' ')[1..2] }
	                        .each_with_object(Hash.new { |h, k| h[k] = Hash.new { |h1, k1| h1[k1] = false } }) { |e, h| h[e[0]][e[1]] = true }

	weights = file.select { |e| e.start_with? 'n' }.map { |e| e.split(' ')[1..2] }

	weights_vector = weights.map { |e| e[1] = e[1].to_f; e }.sort_by { |e| SORT_FACTORS[sort_order]*incompatibilities[e[0]].length } if sort_mode == :incompat 
	weights_vector = weights.map { |e| e[1] = e[1].to_f; e }.sort_by { |e| SORT_FACTORS[sort_order]*e[1] } if sort_mode == :weight

	# weights = weights.reduce({}) { |h, e| h[e[0]] = e[1].to_f; h }

	$log.debug { "incompatibilities = #{incompatibilities.inspect}" }
	$log.debug { "weights = #{weights.inspect}" }
	$log.debug { "weights_vector = #{weights_vector.inspect}" }

	lavados = [{ elems: [], tiempo: 0, tiempo_total: 0 }]
	weights_vector.each do |prenda, tiempo|
		# puts lavados
		# lavados.sort_by! { |e| e[:tiempo] }
		lavados.sort_by! { |e| -1.0*e[:tiempo_total]/e[:elems].length }

		lavados.each_with_index do |lav, i|
			collision = lav[:elems].any? { |e| incompatibilities[prenda][e] || incompatibilities[e][prenda] }

			lavados << { elems: [], tiempo: 0, tiempo_total: 0 } if collision && lavados.length == i + 1
			next if collision

			$log.debug { "Agregada prenda #{prenda} con tiempo #{tiempo} a lavado #{i}" }
			lav[:elems] << prenda
			lav[:tiempo] = [lav[:tiempo], tiempo].max
			lav[:tiempo_total] += tiempo
			break
		end
	end

	$log.debug { "Lavados: #{lavados}" }
	$log.info "Óptimo: #{lavados.map {|e| e[:tiempo] }.reduce(&:+)}"

	File.open("#{__dir__}/solution_#{input_file}", "w") do |file|
		lavados.each_with_index do |lav, i|
			lav[:elems].each { |e| file.puts("#{e} #{i}")}
		end
	end
end

if $PROGRAM_NAME == __FILE__
	# PidFile.new piddir: __dir__
	opts = GetoptLong.new(
	  ['--help', '-h', GetoptLong::NO_ARGUMENT],
	  ['--verbose', '-v', GetoptLong::OPTIONAL_ARGUMENT],
	  ['--debug', '-d', GetoptLong::NO_ARGUMENT],
	  ['--incompat', '-i', GetoptLong::NO_ARGUMENT],
	  ['--weight', '-w', GetoptLong::NO_ARGUMENT],
	  ['--sort_order', '-o', GetoptLong::REQUIRED_ARGUMENT]
	)

	$log = Logger.new(STDOUT)
	$log.sev_threshold = Logger::INFO
	sort_mode = :incompat
	sort_order = :desc

	opts.each do |opt, arg|
		case opt
		when '--verbose'
			$log = Logger.new(STDOUT)
			$log.sev_threshold = Logger::INFO
			$log.sev_threshold = "Logger::#{arg.upcase}".constantize unless arg.blank? || arg =~ /-?d/
			ARGV.unshift '-d' if arg =~ /-?d/
		when '--debug'
			$log.sev_threshold = Logger::DEBUG
		when '--incompat'
			sort_mode = :incompat
		when '--weight'
			sort_mode = :weight
		when '--sort_order'
			sort_order = arg.to_sym
		when '--help'
			puts <<-HELP
				ruby heuristica.rb [OPTIONS] [FILENAME]

				FILENAME -> archive de input

				--verbose [DEBUG|INFO|WARN|ERROR|FATAL] -> Salida por STDOUT
				--debug -> Severidad de logueo = DEBUG
				--incompat -> Ordena por cantidad de incompatibilidades. Default. Default desc.
				--weight -> Ordena por peso. Default desc.
				--sort_order asc|desc -> Tipo de ordenamiento (ascendente o descendente) 
			HELP
			exit 0
		end
	end

	filename = ARGV.shift || 'primer_problema.txt'

	measure('Solved') do
		solve(filename, sort_mode, sort_order)
	end

end

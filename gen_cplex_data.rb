def gen_cplex_data(input_file, qty_lavados)
	file = File.read(input_file).split("\n")
	incompatibilities = file.select {|e| e.start_with? 'e'}
	                        .map { |e| e.split(' ')[1..2] }
	                        .each_with_object(Hash.new { |h, k| h[k] = Hash.new { |h1, k1| h1[k1] = false } }) { |e, h| h[e[0]][e[1]] = true }

	weights = file.select { |e| e.start_with? 'n' }.map { |e| e.split(' ')[1..2] }.sort_by { |e| e.first.to_i }

	prendas = []
	tiempos = []

	weights.each do |p,t|
		prendas << p
		tiempos << t
	end
	puts "PRENDAS = #{prendas.to_s.tr('[]','{}')};"
	puts "TIEMPO_LAVADO = #{tiempos.map(&:to_i)};"

	puts "LAVADOS = #{("0"...qty_lavados).to_a.to_s.tr('[]','{}')};"
	incompat = []
	prendas.each do |i|
		prendas.each do |j|
			incompat[i.to_i] ||= []
			incompat[i.to_i][j.to_i] = incompatibilities[i][j] ? 1 : 0
		end
	end

	incompat.shift
	incompat.map(&:shift)
	puts "INCOMPATIBILIDADES = #{incompat};"
end

if $PROGRAM_NAME == __FILE__
	# Archivo de entrada, cantidad de lavados
	gen_cplex_data ARGV[0], ARGV[1]
end